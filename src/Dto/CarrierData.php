<?php

namespace Altra\ShippyPro\Dto;

use Altra\Dto\DataTransfer;

class CarrierData extends DataTransfer
{
    public function __construct(
    public int $carrierId,
    public string $carrierName,
    public string | null $carrierService,
  ) {
    }

    public static function model(): string
    {
        return '';
    }
}
