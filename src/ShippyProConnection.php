<?php

namespace Altra\ShippyPro;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Http;

class ShippyProConnection
{
    public PendingRequest $request;

    public function __construct()
    {
        $apiKey = base64_encode(config('shippy_pro.api_key').':');

        $this->request = Http::withHeaders([
            'Content-type' => 'application/json',
        ])->withToken($apiKey, 'Basic')->baseUrl(config('shippy_pro.endpoint_url'));
    }
}
