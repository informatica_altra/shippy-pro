## Add shipments functionality


#### Usage

We can create shipments on Shippy Pro like this:

```
ShippyPro::createShipment()
        ->toAddress(ToAddressData::fromArray([
          'name'    => $data['name'],
          'company' => $data['company'],
          'street1' => $data['street_1'],
          'street2' => $data['street_2'],
          'city'    => $data['city'],
          'state'   => $data['state'],
          'zip'     => $data['postcode'],
          'country' => $data['country_iso'],
          'phone'   => $data['phone'],
          'email'   => $data['email'],
        ]))
        ->fromAddress(FromAddressData::fromArray(config('shippy_pro.from_address')))
        ->parcels([Parcels])
        ->carrierNote("Info for the carrier")
        ->carrier(CarrierData::fromArray([
          'carrierName'    => $data['carrier_name'],
          'carrierId'      => $data['carrier_id'],
          'carrierService' => $data['carrier_service'],
        ]))
        ->transactionId($data['transaction_id'])
        ->isReturn($data['is_return'])
        ->contentDescription($data['content_description'])
        ->requestShipment()
      ;
```


We need to make sure that in our shippy_pro.php config we have the from_address setup correctly otherwise we can pass it manually like we do on the toAddress method.
