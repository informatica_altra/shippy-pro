<?php

namespace Altra\ShippyPro\PendingClasses;

use Altra\ShippyPro\Dto\CarrierData;
use Altra\ShippyPro\Dto\Pickups\FromAddressData;
use Altra\ShippyPro\Dto\Pickups\ToAddressData;
use Altra\ShippyPro\ShippyProConnection;

class PendingCreateShipment
{
    public array $toAddress;

    public array $fromAddress;

    public array $parcels;

    public float $totalValue;

    public string $transactionId;

    public string $contentDescription;

    public CarrierData $carrier;

    public string $note = '';

    public bool $isReturn;

    public bool $async = false;

    public string $carrierNote;

    private string $method = 'Ship';

    public function __construct(protected ShippyProConnection $connection)
    {
    }

    public function toAddress(ToAddressData $toAddress): PendingCreateShipment
    {
        $this->toAddress = $toAddress->toArray();

        return $this;
    }

    public function fromAddress(FromAddressData $fromAddress): PendingCreateShipment
    {
        $this->fromAddress = $fromAddress->toArray();

        return $this;
    }

    public function parcels(array $parcels): PendingCreateShipment
    {
        $this->parcels = $parcels;

        return $this;
    }

    public function totalValue(float $totalValue): PendingCreateShipment
    {
        $this->totalValue = $totalValue;

        return $this;
    }

    public function transactionId(string $transactionId): PendingCreateShipment
    {
        $this->transactionId = $transactionId;

        return $this;
    }

    public function contentDescription(string $contentDescription): PendingCreateShipment
    {
        $this->contentDescription = $contentDescription;

        return $this;
    }

    public function carrier(CarrierData $carrier): PendingCreateShipment
    {
        $this->carrier = $carrier;

        return $this;
    }

        public function note(string $note): PendingCreateShipment
        {
            $this->note = $note;

            return $this;
        }

    public function carrierNote(string $carrierNote): PendingCreateShipment
    {
        $this->carrierNote = $carrierNote;

        return $this;
    }

    public function isReturn(bool $isReturn): PendingCreateShipment
    {
        $this->isReturn = $isReturn;

        return $this;
    }

    public function async(bool $async): PendingCreateShipment
    {
        $this->async = $async;

        return $this;
    }

    public function requestShipment()
    {
        $params = $this->params();

        return $this->connection->request->post('', ['Method' => $this->method, 'Params' => $params])->json();
    }

        private function params(): array
        {
            $this->params['to_address']         = $this->toAddress;
            $this->params['from_address']       = $this->fromAddress;
            $this->params['parcels']            = $this->parcels;
            $this->params['TransactionID']      = $this->transactionId;
            $this->params['IsReturn']           = $this->isReturn;
            $this->params['CarrierNote']        = $this->carrierNote;
            $this->params['Async']              = $this->async;
            $this->params['ContentDescription'] = $this->contentDescription;
            $this->params['CarrierName']        = $this->carrier->carrierName;
            $this->params['CarrierID']          = $this->carrier->carrierId;
            $this->params['CarrierService']     = $this->carrier->carrierService;
            $this->params['TotalValue']         = number_format((float) $this->totalValue, 2, '.', '');

            return $this->params;
        }
}
