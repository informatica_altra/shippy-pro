<?php

namespace Altra\ShippyPro\Dto\Pickups;

use Altra\Dto\DataTransfer;

class ParcelData extends DataTransfer
{
    public function __construct(
    public float $length,
    public float $width,
    public float $height,
    public float $weight,
  ) {
    }

    public static function model(): string
    {
        return '';
    }
}
