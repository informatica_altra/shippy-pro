<?php

namespace Altra\ShippyPro;

use Altra\ShippyPro\PendingClasses\PendingBookPickup;
use Altra\ShippyPro\PendingClasses\PendingCreateShipment;
use stdClass;

class ShippyProClient
{
    protected ShippyProConnection $connection;

    public function __construct()
    {
        $this->connection = new ShippyProConnection();
    }

    public function getCarriers()
    {
        return $this->connection->request->post('', ['Method' => 'GetCarriers', 'Params' => new stdClass()])->json();
    }

    public function getPickups(array $pickup = [])
    {
        return $this->connection->request->post('', ['Method' => 'GetPickups', 'Params' => new stdClass($pickup)])->json();
    }

    public function cancelPickup(array $pickup)
    {
        return $this->connection->request->post('', ['Method' => 'CancelPickup', 'Params' => $pickup])->json();
    }

    public function checkAddress(array $address)
    {
        return $this->connection->request->post('', ['Method' => 'CheckAddress', 'Params' => $address])->json();
    }

    public function getLabel(int $orderId)
    {
        return $this->connection->request->post('', ['Method' => 'GetLabelUrl', 'Params' => $orderId])->json();
    }

    public function createShipment()
    {
        return new PendingCreateShipment($this->connection);
    }

    public function bookPickup()
    {
        return new PendingBookPickup($this->connection);
    }
}
