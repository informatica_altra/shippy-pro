<?php

return [
    'endpoint_url' => env('SHIPPY_PRO_ENDPOINT_URL'),
    'api_key' => env('SHIPPY_PRO_API_KEY'),

    // Address that should be used for shipments from your company (optional)
    'from_address' => [
        'name' => 'John Doe',
        'company' => 'Company name',
        'street1' => '',
        'street2' => '',
        'city' => '',
        'state' => 'City code (like RO from Rome)',
        'zip' => '',
        'country' => 'COUNTRY code (like ES for Spain)',
        'phone' => '',
        'email' => '',
    ],
];
