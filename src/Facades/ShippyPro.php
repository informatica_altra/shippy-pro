<?php

namespace Altra\ShippyPro\Facades;

use Altra\ShippyPro\ShippyProClient;
use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\Http;

class ShippyPro extends Facade
{
    protected static function getFacadeAccessor()
    {
        return ShippyProClient::class;
    }

    public static function fake($callback)
    {
        $endpoint = config('shippy_pro.endpoint_url').'/';

        $responses = $callback(static::fakeData());
        $sequence = Http::sequence();
        foreach ($responses as $response) {
            $sequence->push($response, 200);
        }

        Http::fake([
            $endpoint => $sequence,
        ]);
    }

    private static function fakeData()
    {
        return [
            'carriers' => [
                'Carriers' => [
                    'DHLExpress' => [
                        [
                            'CarrierID' => '15101',
                            'CarrierService' => 'EXPRESS DOMESTIC',
                            'ServicesList' => [
                                'EXPRESS DOMESTIC',
                                'DOMESTIC EXPRESS',
                                'EXPRESS DOMESTIC 12=>00',
                                'EXPRESS DOMESTIC 10=>30',
                                'EXPRESS DOMESTIC 9=>00',
                                'ECONOMY SELECT EU',
                                'ECONOMY SELECT',
                                'ECONOMY SELECT NONDOC',
                                'EXPRESS WORLDWIDE EU',
                                'EXPRESS WORLDWIDE DOC',
                                'EXPRESS WORLDWIDE NONDOC',
                                'EXPRESS 12=>00 DOC',
                                'EXPRESS 12=>00 NONDOC',
                                'EXPRESS 9=>00 DOC',
                                'EXPRESS 10=>30 NONDOC',
                            ],
                            'Label' => 'TEST_DHL_EXPRESS',
                            'CarrierLogo' => 'https://cdn.shippypro.com/assets/images/carriers/dhlexpress.png',
                        ],
                    ],
                ],
            ],
            'cancelPickup' => [
                'Result' => 'OK',
                'Message' => 'Booking Cancelled',
            ],
        ];
    }
}
