<?php

namespace Altra\ShippyPro;

use Illuminate\Support\ServiceProvider;

class ShippyProServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/shippy_pro.php' => config_path('shippy_pro.php'),
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
