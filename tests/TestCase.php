<?php

namespace Altra\ShippyPro\Tests;

use Altra\ShippyPro\ShippyProServiceProvider;
use Orchestra\Testbench\TestCase as Orchestra;

class TestCase extends Orchestra
{
    protected function setUp(): void
    {
        parent::setUp();

        config(['shippy_pro.endpoint_url' => 'https://www.shippypro.com/api/v1']);
        config(['shippy_pro.api_key' => 'test']);
        $this->createDummyprovider()->boot();
    }

      /**
       * @throws \ReflectionException
       */
      protected function createDummyprovider(): object
      {
          $reflectionClass = new \ReflectionClass(ShippyProServiceProvider::class);

          return $reflectionClass->newInstanceWithoutConstructor();
      }
}
