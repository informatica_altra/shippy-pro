# Laravel Shippy Pro

Package to connect Shippy Pro API to Laravel.

#### Installation

```

composer require altra/shippy-pro

php artisan vendor:publish
```

#### Usage

###### Get Carriers:

This will list all the carriers that you have on Shippy Pro

```
ShippyPro::getCarriers()
```

###### Get Pickups:

This will list all the pickups you have queued or just one pickup if yo pass the paramater $pickup$

```
ShippyPro::getPickups();
OR
ShippyPro::getPickups(['PickupId' => {ID}]);
```

###### Book a Pickup:

You can book a pickup

```
ShippyPro::bookPickup()
->toAddress({ADDRESSFORPICKUPTOARRIVE})
->fromAddress({ADDRESSTOPICKUP})
->parcels({ARRAYOFPARCELSTOPICKUP})
->carrier({CARRIERTOUSE}})
->pickupTime({DATEANDTIMETOPICKUP})
->requestPickup();
```

There are some optional methods as well like:

```
->note() // Add a string for an observation
->morningMinTime() // add a string like "08:00"
->morningMaxTime() // add a string like "12:00"
->afternoonMinTime() // add a string like "14:00"
->afternoonMaxTime() // add a string like "18:00"
```

###### Cancel a Pickup:

```
ShippyPro::cancelPickup({PICKUPTOCANCEL});
```

###### Check Address Validation:

```
ShippyPro::checkAddress({ADDRESSTOVALIDATE});
```

###### Create shipment

We can create shipments on Shippy Pro like this:

```
ShippyPro::createShipment()
        ->toAddress(ToAddressData::fromArray([
          'name'    => $data['name'],
          'company' => $data['company'],
          'street1' => $data['street_1'],
          'street2' => $data['street_2'],
          'city'    => $data['city'],
          'state'   => $data['state'],
          'zip'     => $data['postcode'],
          'country' => $data['country_iso'],
          'phone'   => $data['phone'],
          'email'   => $data['email'],
        ]))
        ->fromAddress(FromAddressData::fromArray(config('shippy_pro.from_address'))) // comes from the config but you can do it manually like toAddress
        ->parcels([Parcels]) // array of parcels
        ->carrierNote("Info for the carrier") // Note for the carrier
        ->carrier(CarrierData::fromArray([
          'carrierName'    => $data['carrier_name'],
          'carrierId'      => $data['carrier_id'],
          'carrierService' => $data['carrier_service'],
        ]))
        ->transactionId($data['transaction_id']) // Your personal ID
        ->isReturn($data['is_return']) // bool
        ->contentDescription($data['content_description']) // Description of the parcel
        ->requestShipment()
      ; 
```

We need to make sure that in our shippy_pro.php config we have the from_address setup correctly otherwise we can pass it manually like we do on the toAddress method.
