<?php

namespace Altra\ShippyPro\Dto\Pickups;

use Altra\Dto\DataTransfer;

class FromAddressData extends DataTransfer
{
    public function __construct(
    public string $name,
    public string | null $company,
    public string $street1,
    public string | null $street2,
    public string $city,
    public string $state,
    public string $zip,
    public string $country,
    public string $phone,
    public string $email,
  ) {
    }

    public static function model(): string
    {
        return '';
    }
}
