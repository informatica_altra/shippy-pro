<?php

namespace Altra\ShippyPro\PendingClasses;

use Altra\ShippyPro\Dto\CarrierData;
use Altra\ShippyPro\Dto\Pickups\FromAddressData;
use Altra\ShippyPro\Dto\pickups\ToAddressData;
use Altra\ShippyPro\ShippyProConnection;
use Carbon\Carbon;

class PendingBookPickup
{
    public array $toAddress;

    public array $fromAddress;

    public array $parcels;

    public string $note = '';

    public int $pickupTime;

    public string $morningMinTime = '08:00';

    public string $morningMaxTime = '12:00';

    public string $afternoonMinTime = '14:00';

    public string $afternoonMaxTime = '18:00';

    public CarrierData $carrier;

    private string $method = 'BookPickup';

    public array $params = [];

    public function __construct(protected ShippyProConnection $connection)
    {
    }

    public function toAddress(ToAddressData $toAddress): PendingBookPickup
    {
        $this->toAddress = $toAddress->toArray();

        return $this;
    }

    public function fromAddress(FromAddressData $fromAddress): PendingBookPickup
    {
        $this->fromAddress = $fromAddress->toArray();

        return $this;
    }

    public function parcels(array $parcels): PendingBookPickup
    {
        $this->parcels = $parcels;

        return $this;
    }

    public function note(string $note): PendingBookPickup
    {
        $this->note = $note;

        return $this;
    }

    public function pickupTime(Carbon $pickupTime): PendingBookPickup
    {
        $this->pickupTime = $pickupTime->timestamp;

        return $this;
    }

    public function morningMinTime(string $morningMinTime): PendingBookPickup
    {
        $this->morningMinTime = $morningMinTime;

        return $this;
    }

    public function morningMaxTime(string $morningMaxTime): PendingBookPickup
    {
        $this->morningMaxTime = $morningMaxTime;

        return $this;
    }

    public function afternoonMinTime(string $afternoonMinTime): PendingBookPickup
    {
        $this->afternoonMinTime = $afternoonMinTime;

        return $this;
    }

    public function afternoonMaxTime(string $afternoonMaxTime): PendingBookPickup
    {
        $this->afternoonMaxTime = $afternoonMaxTime;

        return $this;
    }

    public function carrier(CarrierData $carrier): PendingBookPickup
    {
        $this->carrier = $carrier;

        return $this;
    }

    public function requestPickup()
    {
        $params = $this->params();

        return $this->connection->request->post('', ['Method' => $this->method, 'Params' => $params])->json();
    }

    private function params(): array
    {
        $this->params['to_address'] = $this->toAddress;
        $this->params['from_address'] = $this->fromAddress;
        $this->params['parcels'] = $this->parcels;
        $this->params['PickupNote'] = $this->note;
        $this->params['CarrierName'] = $this->carrier->carrierName;
        $this->params['CarrierID'] = $this->carrier->carrierId;
        $this->params['PickupTime'] = $this->pickupTime;
        $this->params['PickupMorningMintime'] = $this->morningMinTime;
        $this->params['PickupMorningMaxtime'] = $this->morningMaxTime;
        $this->params['PickupAfternoonMintime'] = $this->afternoonMinTime;
        $this->params['PickupAfternoonMaxtime'] = $this->afternoonMaxTime;

        return $this->params;
    }
}
